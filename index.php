<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';

use Chess\Board;
use Chess\Piece;
use Chess\PluginManager;
use Chess\Hooks;
use Chess\Hooks\HookExample;



$redis_client = new Predis\Client();
$pm = new PluginManager();
$pm->register(new HookExample("board_add"));
$board = new Board();
$board->show();
while(true) {
    $input = explode(' ', trim(readline('Enter a command (help or h for help): ')));
    switch($input[0]){
        case 'exit':
        case 'quit':
        case 'q':
            return;
            break;
        case 'help':
        case 'h':
        default:
            printHelp();
            break;
        case 'add':
            if(!isset($input[1]) || !isset($input[2])){
                printHelp();
                break;
            }
            $board->add(new Piece($input[1]), $input[2]);
            $board->show();
            $pm->emitEvent("board_add");
            break;
        case 'move':
        case 'mv':
            if(!isset($input[1]) || !isset($input[2])){
                printHelp();
                break;
            }
            $board->move($input[1], $input[2]);
            $board->show();
            $pm->emitEvent("board_move");
            break;
        case 'rm':
        case 'remove':
            if(!isset($input[1])){
                printHelp();
                break;
            }
            $board->remove($input[1]);
            $board->show();
            $pm->emitEvent("board_remove");
            break;
        case 'save':
            if(strtolower($input[1])==='redis'){
                $board_dump = serialize($board);
                $redis_client->set('board_dump', $board_dump);
            }
            elseif(strtolower($input[1])=='file' && isset($input[2])) {
                file_put_contents($input[2], serialize($board));
            }
            else{
                printHelp();
            }
            break;
        case 'load':
            if(strtolower($input[1])==='redis'){
                $board_dump = $redis_client->get('board_dump');
                $board = unserialize($board_dump);
            }
            elseif(strtolower($input[1])=='file' && isset($input[2])) {
                $board_dump = file_get_contents($input[2]);
                $board = unserialize($board_dump);
            }
            else{
                printHelp();
            }
            $board->show();
            break;
        case 'dump':
            var_dump($board);
            break;
    }
    //echo chr(27).chr(91).'H'.chr(27).chr(91).'J'; //clear screen
}

function printHelp(){
    echo "exit or quit or q for exit\n";
    echo "help for this\n";
    echo "save for save the boards state (example: \"save redis\" or \"save file file_name.txt\")\n";
    echo "load for load the boards state (example: \"load redis\" or \" load file file_name.txt\")\n";
    echo "add for add a Piece(example: 'add king d1')\n";
    echo "move for move a Piece(example: 'move d1 e2')\n";
    echo "rm for remove a Piece(example: 'rm e2')\n";
}