<?php
declare(strict_types=1);

namespace Chess;

use Chess\Piece;

class Board
{
    private $field = [];

    public function __construct()
    {
        for ($i = 1; $i < 9; $i++) {
            for ($y = 'a'; $y < 'i'; $y++) {
                $this->field[$i][$y] = null;
            }
        }
    }

    public function show()
    {
        $r = 8;
        foreach (array_reverse($this->field) as $row => $columns) {
            echo $r . ' ';
            foreach ($columns as $column) {
                if ($column instanceof Piece) {
                    echo $column;
                } else {
                    echo ' ';
                }
            }
            echo "\n";
            $r--;
        }
        echo '  ';
        for($i='a'; $i < 'i'; $i++){
            echo $i;
        }
        echo "\n";
    }

    public function add(Piece $piece, string $dest)
    {
        list($r, $c) = $this->parsePath($dest);
        $this->field[$r][$c] = $piece;
    }

    public function move(string $from, string $to)
    {
        $arrFrom = str_split($from);
        $arrTo = str_split($to);
        $this->field[$arrTo[1]][$arrTo[0]] = $this->field[$arrFrom[1]][$arrFrom[0]];
        $this->field[$arrFrom[1]][$arrFrom[0]] = null;
    }

    public function remove(string $dest)
    {
        list($r, $c) = $this->parsePath($dest);
        $this->field[$r][$c] = null;
    }

    private function parsePath(string $ld): array
    {
        $ld = trim($ld);
        $res = array_reverse(str_split($ld));
        return $res;
    }

    public function getPiece(string $ld){
        list($r, $c) = $this->parsePath($ld);
        return $this->field[$r][$c];
    }
}
