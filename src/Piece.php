<?php
declare(strict_types=1);

namespace Chess;


class Piece
{
    private $name;
    public function __construct(string $name)
    {
        $this->name = $name;
    }
    public function __toString(): string
    {
        return str_split($this->name)[0];
    }
}
