<?php
namespace Chess;

use Chess\Hooks\HookBase;

class PluginManager {
    private $pluginStorage = [];

    public function emitEvent(string $eventName) {
        foreach($this->pluginStorage[$eventName] as $hook) {
            $hook->call();
        }
    }

    public function register(HookBase $hook, string $eventName = '') {
        if($eventName === '') {
            $eventName = $hook->getEventName();
        }
        if(!array_key_exists($eventName, $this->pluginStorage)) {
            $this->pluginStorage[$eventName] = [];
        }
        array_push($this->pluginStorage[$eventName], $hook);
    }
}