<?php 

namespace Chess\Hooks;

abstract class HookBase {
    protected $eventName;
    abstract public function call();
    abstract public function __construct($eventName);
    abstract public function getEventName(): string;
}