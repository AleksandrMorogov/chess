<?php
namespace Chess\Hooks;

class HookExample extends HookBase {
    public function __construct($eventName) {
        $this->eventName = $eventName;
    }
    public function call() {
        echo "Hello from ExampleHook!\n";
    }
    public function getEventName(): string{
        return $this->eventName;
    }
}