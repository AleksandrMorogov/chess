<?php
// declare(strict_types=1);

namespace Chess\Tests;

use PHPUnit\Framework\TestCase;
use Chess\Board;
use Chess\Piece;

class BoardTest extends TestCase {
    private $board;

    function setUp(){
        $this->board = new Board();
    }

    public function testAddPieceOutsideTheBoard(){
        $this->board->add(new Piece("testPiece"), 'b9');
        $this->assertInstanceOf(Piece::class, $this->board->getPiece('b9'));
    }
}